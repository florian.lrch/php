# Projet PHP

Projet Universitaire - Création d'un site internet sous PHP  
Introduction à l'objet en PHP.

Projet réalisé avec Néreis Dugaleix.

Structure du projet :

![Structure du projet](/structure_capture.PNG)

## Technologies utilisées

### Langages

* HTML
* CSS
* PHP

### Base de données

* MySQL

## Capture d'écran

Interface public

![public](/interface_public.PNG)

Interface administration 

![administration](/interface_admin.PNG)
